// MARK: - Data structures declarations
// MARK: Enumeration for suits of cards
enum Suit : CaseIterable {
    case Hearts, Tiles, Clovers, Pikes
}

// MARK: Enumeration for ranks of cards
enum CardRank : Int, CaseIterable {
    case Six = 6, Seven, Eight, Nine, Ten
    case Jack, Queen, King, Ace
}

// MARK: The struct that describes a card
struct Card : CustomStringConvertible {
    let suit: Suit
    let rank: CardRank

    var description: String {
        let charForSuit: Character
        switch suit {
            case .Hearts: charForSuit = "♥"
            case .Pikes: charForSuit = "♠"
            case .Tiles: charForSuit = "♦"
            case .Clovers: charForSuit = "♣"
        }

        let rankDescription: String
        if (rank.rawValue < CardRank.Ten.rawValue) {
            let startingValue = Int(UnicodeScalar("0").value)
            rankDescription = String(UnicodeScalar(rank.rawValue + startingValue)!)
        } else {
            switch rank {
                case.Ten: rankDescription = "10"
                case .Jack: rankDescription = "J"
                case .Queen: rankDescription = "Q"
                case .King: rankDescription = "K"
                default: rankDescription = "A"
            }
        }

        return "\(rankDescription)\(charForSuit)"
    }
}

// MARK: Enumeration for possible error during initialization card combination
enum CardCombinationError : Error {
    case WrongNumOfCards, NotOnePackOfCards
}

class CardCombination {
    private enum Combination : String, CaseIterable {
        case HighCard // There isn't combination
        case OnePair = "one pair" // Two cards with the same CardRank
        case TwoPair = "two pair" // Two OnePair
        case ThreeOfAKind = "three of a kind" // Three cards with the same CardRank
        case Straight = "straight" // Five cards of sequential rank
        case Flush = "flush" // Five cards all of the same suit
        case FullHouse = "full house" // Three cards of one rank and two cards of another rank
        case FourOfAKind = "four of a kind" // Four cards of one rank and one card of another rank
        case StraightFlush = "straight flush" // Five cards of sequential rank, all of the same suit
    }

    private static let CARDS_IN_COMB = 5
    private static let combinationsHandlers: [Combination : ([Card]) -> Bool] = [
        .HighCard: isHighCard,
        .OnePair: isOnePair,
        .TwoPair: isTwoPair,
        .ThreeOfAKind: isThreeOfAKind,
        .Straight: isStraight,
        .Flush: isFlush,
        .FullHouse: isFullHouse,
        .FourOfAKind: isFourOfAKind,
        .StraightFlush: isStraightFlush,
    ]

    let cards: [Card]

    init() {
        var cards = [Card]()
        cards.reserveCapacity(CardCombination.CARDS_IN_COMB)

        let ranks = [CardRank](CardRank.allCases.shuffled().prefix(CardCombination.CARDS_IN_COMB))
        for rank in ranks {
            let suit = Suit.allCases.randomElement()!
            cards.append(Card(suit: suit, rank: rank))
        }

        self.cards = cards
    }

    init(cards: [Card]) throws {
        guard cards.count == CardCombination.CARDS_IN_COMB else {
            throw CardCombinationError.WrongNumOfCards
        }

        let cardsBySuit = Dictionary(grouping: cards) {
            $0.suit
        }
        for (_, cardsWithSameSuit) in cardsBySuit {
            let ranks = cardsWithSameSuit.map { $0.rank }
            let uniqueRanks = Set(ranks)

            guard ranks.count == uniqueRanks.count else {
                throw CardCombinationError.NotOnePackOfCards
            }
        }

        self.cards = cards
    }

    func getCombination() -> String {
        let combinationToFind = Combination.allCases.dropFirst().reversed()

        for combination in combinationToFind {
            if CardCombination.combinationsHandlers[combination]!(cards) {
                return makeMessage(combination: combination)
            }
        }

        return makeMessage(combination: Combination.HighCard)
    }

    private func makeMessage(combination: Combination) -> String {
        "You got \(combination.rawValue)!"
    }

    private static func isHighCard(cards: [Card]) -> Bool {
        true
    }

    private static func isOnePair(cards: [Card]) -> Bool {
        countGroupOfRanks(cards: cards, sizeOfGroup: 2) == 1
    }

    private static func isTwoPair(cards: [Card]) -> Bool {
        countGroupOfRanks(cards: cards, sizeOfGroup: 2) == 2
    }

    private static func isThreeOfAKind(cards: [Card]) -> Bool {
        countGroupOfRanks(cards: cards, sizeOfGroup: 3) == 1
    }

    private static func isStraight(cards: [Card]) -> Bool {
        let ranks = cards
                        .map { $0.rank.rawValue }
                        .sorted(by: <)

        var next = ranks.first!
        return ranks.allSatisfy {
            defer { next += 1 }
            return $0 == next
        }
    }

    private static func isFlush(cards: [Card]) -> Bool {
        cards.map { $0.suit }.allSatisfy { $0 == cards.first!.suit }
    }

    private static func isFullHouse(cards: [Card]) -> Bool {
        isThreeOfAKind(cards: cards) && isOnePair(cards: cards)
    }

    private static func isFourOfAKind(cards: [Card]) -> Bool {
        countGroupOfRanks(cards: cards, sizeOfGroup: 4) == 1
    }

    private static func isStraightFlush(cards: [Card]) -> Bool {
        isFlush(cards: cards) && isStraight(cards: cards)
    }

    private static func countGroupOfRanks(cards: [Card], sizeOfGroup: Int) -> Int {
        let ranks = cards.map { $0.rank.rawValue }
        let numOfEqRanks = Dictionary(grouping: ranks) { $0 }

        return numOfEqRanks.lazy.filter({ $0.value.count == sizeOfGroup }).count
    }
}

// MARK: - Tests
// MARK: Random cards combinations
print("Random combinations:")
for _ in 0..<5 {
    let combination = CardCombination()
    print(combination.cards)
    print(combination.getCombination())
    print()

}

// MARK: Test wrong combinations
var myCards = [
    Card(suit: .Hearts, rank: .Ace),
    Card(suit: .Clovers, rank: .Ace),
    Card(suit: .Pikes, rank: .Ace),
    Card(suit: .Tiles, rank: .Ace),
    Card(suit: .Hearts, rank: .Ace),
]
do {
    let _ = try CardCombination(cards: myCards)
    fatalError("Must be wrong combination!")
} catch CardCombinationError.NotOnePackOfCards {
}

myCards = [
    Card(suit: .Hearts, rank: .Ace),
    Card(suit: .Clovers, rank: .Ace),
    Card(suit: .Pikes, rank: .Ace),
    Card(suit: .Tiles, rank: .Ace),
    Card(suit: .Hearts, rank: .Queen),
    Card(suit: .Hearts, rank: .Jack),
]
do {
    let _ = try CardCombination(cards: myCards)
    fatalError("Must be wrong combination!")
} catch CardCombinationError.WrongNumOfCards {
}

// MARK: Test specific combinations
// MARK: Function to print test case
func printCombination(cards: [Card]) {
    print(cards)
    let combination = try! CardCombination(cards: cards)
    print(combination.getCombination())
}

print("\n\nSpecific combinations:")
// MARK: One pair
myCards = [
    Card(suit: .Hearts, rank: .Ace),
    Card(suit: .Clovers, rank: .Ace),
    Card(suit: .Pikes, rank: .Jack),
    Card(suit: .Tiles, rank: .Six),
    Card(suit: .Hearts, rank: .Queen),
]
printCombination(cards: myCards)

// MARK: Two pairs
myCards = [
    Card(suit: .Hearts, rank: .Ace),
    Card(suit: .Clovers, rank: .Ace),
    Card(suit: .Pikes, rank: .Jack),
    Card(suit: .Tiles, rank: .Jack),
    Card(suit: .Hearts, rank: .Queen),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Three of a kind
myCards = [
    Card(suit: .Hearts, rank: .Ace),
    Card(suit: .Clovers, rank: .Ace),
    Card(suit: .Pikes, rank: .Ace),
    Card(suit: .Tiles, rank: .Jack),
    Card(suit: .Hearts, rank: .Queen),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Straight
myCards = [
    Card(suit: .Hearts, rank: .Ten),
    Card(suit: .Clovers, rank: .Nine),
    Card(suit: .Pikes, rank: .Eight),
    Card(suit: .Tiles, rank: .Seven),
    Card(suit: .Hearts, rank: .Six),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Flush
myCards = [
    Card(suit: .Hearts, rank: .Ten),
    Card(suit: .Hearts, rank: .Jack),
    Card(suit: .Hearts, rank: .Eight),
    Card(suit: .Hearts, rank: .King),
    Card(suit: .Hearts, rank: .Six),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Full house
myCards = [
    Card(suit: .Hearts, rank: .Ten),
    Card(suit: .Pikes, rank: .Ten),
    Card(suit: .Tiles, rank: .Queen),
    Card(suit: .Clovers, rank: .Ten),
    Card(suit: .Hearts, rank: .Queen),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Four of a kind
myCards = [
    Card(suit: .Hearts, rank: .Ten),
    Card(suit: .Pikes, rank: .Ten),
    Card(suit: .Tiles, rank: .Ten),
    Card(suit: .Clovers, rank: .Ten),
    Card(suit: .Hearts, rank: .Queen),
]
print() // To separate the output
printCombination(cards: myCards)

// MARK: Straight flush
myCards = [
    Card(suit: .Hearts, rank: .Ten),
    Card(suit: .Hearts, rank: .Nine),
    Card(suit: .Hearts, rank: .Seven),
    Card(suit: .Hearts, rank: .Eight),
    Card(suit: .Hearts, rank: .Jack),
]
print() // To separate the output
printCombination(cards: myCards)
