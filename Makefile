OUTPUT	:= "./output/"

SRC1		:= "task5_part1.swift"
NAME_PART1	:= "task5_part1"

SRC2		:= "task5_part2_poker.swift"
NAME_PART2	:= "task5_part2_poker"

all: output_dir $(NAME_PART1) $(NAME_PART2)
	@echo Compilation is successful!

output_dir:
	@test -d $(OUTPUT) || mkdir $(OUTPUT)

$(NAME_PART1):
	@swiftc -O $(SRC1) -o $(OUTPUT)/$(NAME_PART1)

$(NAME_PART2):
	@swiftc -O $(SRC2) -o $(OUTPUT)/$(NAME_PART2)

clean:
	@test -d $(OUTPUT) && rm -rf $(OUTPUT)
