// MARK: - Create a parent class and two children classes
class Parent { }
class Child1 : Parent { }
class Child2 : Parent { }


// MARK: - Create House class with some properties and methods
class House {
    let width: UInt
    let height: UInt

    init(width: UInt, height: UInt) {
        self.width = width
        self.height = height
    }

    func create() -> UInt { return width * height }
    func destroy() { print("House was destroyed!") }
}


// MARK: - Create a class that sorts array of pupils by different parameters
// MARK: Enum for possible initialization errors
enum PupilInitError : Error {
    case AgeBoundsError, AgeInFirstClassError
}

// MARK: Enum for types of schools
enum TypeOfSchool : String, CaseIterable {
    case Private, Public
}

// MARK: Class that describe a pupil
class Pupil : CustomStringConvertible {
    static let MIN_AGE: UInt8 = 6
    static let MAX_AGE: UInt8 = 18
    static let MIN_AGE_IN_1_CLASS: UInt8 = 6
    static let MAX_AGE_IN_1_CLASS: UInt8 = 8

    let firstName: String
    let lastName: String
    let age: UInt8
    let grade: UInt8
    let typeOfSchool: TypeOfSchool

    init(firstName: String, lastName: String, age: UInt8,
         ageInFirstGrade: UInt8, typeOfSchool: TypeOfSchool = .Public) throws {
        guard Pupil.MIN_AGE <= age && age <= Pupil.MAX_AGE else {
            throw PupilInitError.AgeBoundsError
        }
        guard Pupil.MIN_AGE_IN_1_CLASS <= ageInFirstGrade &&
                ageInFirstGrade <= Pupil.MAX_AGE_IN_1_CLASS else {
            throw PupilInitError.AgeInFirstClassError
        }

        self.firstName = firstName
        self.lastName = lastName
        self.age = age
        self.grade = age - ageInFirstGrade + 1
        self.typeOfSchool = typeOfSchool
    }

    var description: String {
        """
        \(lastName) \(firstName):
            * Age -- \(age)
            * Grade -- \(grade)
            * Type of school -- \(typeOfSchool)
        """
    }
}

// MARK: Array of pupils
let firstNames = ["Bob", "Anna", "Alex", "Lucy", "Jacob", "Emily",
                            "Michael", "Emma", "Joshua", "Madison"]
let lastNames = ["Smith", "Jones", "Williams", "Brown", "Taylor",
                           "Davies", "Evans", "Thomas", "Johnson", "Wright"]
var pupils = [Pupil]()
for _ in 0..<2 {
    pupils.append(
        try Pupil(
            firstName: firstNames.randomElement()!,
            lastName: lastNames.randomElement()!,
            age: UInt8.random(in: Pupil.MIN_AGE...Pupil.MAX_AGE),
            ageInFirstGrade: UInt8.random(in: Pupil.MIN_AGE_IN_1_CLASS...Pupil.MAX_AGE_IN_1_CLASS),
            typeOfSchool: TypeOfSchool.allCases.randomElement()!
        )
    )
}

// MARK: Class that sorts array of pupils
class Sorter {
    static func sortByLastName(pupils: inout [Pupil]) {
        uniformSort(pupils: &pupils) { $0.lastName < $1.lastName }
    }

    static func sortByFirstName(pupils: inout [Pupil]) {
        uniformSort(pupils: &pupils) { $0.firstName < $1.firstName }
    }

    static func sortByAge(pupils: inout [Pupil]) {
        uniformSort(pupils: &pupils) { $0.age < $1.age }
    }

    static func sortByGrade(pupils: inout [Pupil]) {
        uniformSort(pupils: &pupils) { $0.grade < $1.grade }
    }

    static func sortByTypeOfSchool(pupils: inout [Pupil]) {
        uniformSort(pupils: &pupils) {
            $0.typeOfSchool.rawValue < $1.typeOfSchool.rawValue
        }
    }

    private static func uniformSort(pupils: inout [Pupil], compare: (Pupil, Pupil) -> Bool) {
        pupils.sort(by: compare)
    }
}

// MARK: Sort by lastName
print("Sort by last name:")
Sorter.sortByLastName(pupils: &pupils)
pupils.forEach { print($0) }

// MARK: Sort by firstName
print("\nSort by first name:")
Sorter.sortByFirstName(pupils: &pupils)
pupils.forEach { print($0) }

// MARK: Sort by age
print("\nSort by age:")
Sorter.sortByAge(pupils: &pupils)
pupils.forEach { print($0) }

// MARK: Sort by grade
print("\nSort by grade:")
Sorter.sortByGrade(pupils: &pupils)
pupils.forEach { print($0) }

// MARK: Sort by a type of the school
print("\nSort by a type of the school:")
Sorter.sortByTypeOfSchool(pupils: &pupils)
pupils.forEach { print($0) }


// MARK: - Create struct and class and explain their difference
struct PointStruct {
    var x: Int
    var y: Int
}
class PointClass {
    var x: Int
    var y: Int

    init(x: Int, y: Int) {
        self.x = x
        self.y = y
    }
}

/*
    Struct is value type. Class is reference type. It means that when we assign
    one struct for another one, swift make copy. When we assign a class object to
    another variable swift copy not all class but only reference to it.

    Because of it, you can't change 'var' properties of 'let' struct object as
    this action makes object change. And you can change 'var' properties of 'let'
    class object as constant only reference to class.

    Structs get an memberwise initializator by default.
    Structs can't inheret from other structs or classes.
    Structs can't have deinitializers.
    Structs don't have type casts.
*/

let pointStruct = PointStruct(x: 5, y: 3)
let pointClass = PointClass(x: 3, y: 5)
